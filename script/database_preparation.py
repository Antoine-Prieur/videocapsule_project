# --------------------------------------
#               Projet 2A
#
# Author : Antoine Prieur
#
# Creation : 30/04/19
#
# File : database_preparation.py
#
# Last modification : Antoine Prieur
#
# Prepare the dataset for YoloV3 : create bbox using the masks
#
# ------------------------------------------

import cv2
import numpy as np
import os
import sys
import argparse

import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import matplotlib.patches as patches


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("gt_path", help="path to the masks")
    parser.add_argument("object_class", help="class of the object", type=int)
    parser.add_argument("--dest", help="path to the destination of the .txt files", default="../res/yolov3/preparation/")
    args = parser.parse_args()

    error_flag = 0
    if not os.path.exists(args.gt_path):
        print("The path " + args.gt_path + " does not exist")
        error_flag = 1
    if not os.path.exists(args.dest):
        error_flag = 1
        print("The path " + args.dest + " does not exist")
    if error_flag:
        sys.exit(1)
    return args


def img_characteristics(x_blob, y_blob, global_height, global_width):

    """

    Returns the center, the width and the height of all the blobs in the mask we're working on

    :param x_blob: tab of all the x-coordinates of the blob we're working on
    :param y_blob: tab of all the y-coordinates of the blob we're working on
    :return: list of coordinates of the center, the width and the height cX, cY, width, height

    """

    characteristics = []
    for i in range(len(x_blob)):
        if (len(x_blob[i]) != 0) & (len(x_blob[i]) != 0):
            x = x_blob[i]
            y = y_blob[i]
            width = max(x) - min(x)
            height = max(y) - min(y)
            cX = min(x) + width / 2
            cY = min(y) + height / 2
            characteristics.append([cX/global_width, cY/global_height, width/global_width, height/global_height])
    return characteristics


def draw_parameters(img, x_blob, y_blob):
    fig, ax = plt.subplots(1)
    ax.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
    # ax.imshow(mask)
    # point = patches.Circle((cX, cY), 3, edgecolor='r', facecolor='r')
    for i in range(len(x_blob)):

        x = x_blob[i]
        y = y_blob[i]
        width = max(x) - min(x)
        height = max(y) - min(y)
        cX = min(x) + width / 2
        cY = min(y) + height / 2
        rect = patches.Rectangle((cX - width/2, cY - height/2), width, height, linewidth=2, edgecolor='r',
                                 facecolor='none')
        ax.add_patch(rect)
    # ax.add_patch(point)
    plt.show()


def binary_img(img):

    """

    Returns a matrix where all the 0s ar 255 and the other values become 0.
    Used for the SimpleBlobDetector because of a bug
    :param img: matrix (w, h)
    :return: matrix (w, h)

    """

    (x, y) = img.shape
    for i in range(x):
        for j in range(y):
            if img[i][j] > 0:
                img[i][j] = 0
            else:
                img[i][j] = 255
    return img


def process(gt_path, object_class, dest):

    """

    Creates a text file for each images of the data_path containing the informations YoloV3 needs to launch a training


    """

    list_dir = os.listdir(gt_path)
    files_number = len(list_dir)

    print("There is", files_number, "masks to treat")

    # -- Progress bar -- #

    toolbar_width = 50
    sys.stdout.write("[%s]" % (" " * toolbar_width))
    sys.stdout.flush()
    sys.stdout.write("\b" * (toolbar_width + 1))
    bar_progress = 0

    # ----

    empty_files_count = 0

    for i, filename in enumerate(list_dir):
        mask_path = os.path.join(gt_path, filename)
        _, file_extension = os.path.splitext(mask_path)
        mask = cv2.imread(mask_path, cv2.IMREAD_GRAYSCALE)

        params = cv2.SimpleBlobDetector_Params()
        params.filterByInertia = False
        params.filterByConvexity = False
        params.filterByCircularity = False
        params.filterByArea = True
        params.minArea = 100
        params.maxArea = 331776
        params.blobColor = 0
        detector = cv2.SimpleBlobDetector_create(params)
        keyPoints = detector.detect(binary_img(mask))
        if len(keyPoints) == 0:
            empty_files_count += 1
        h, w = mask.shape[:2]

        new_val = 1
        x_blob = []
        y_blob = []

        for j in keyPoints:
            cX, cY = j.pt
            mask_flood = np.zeros((h + 2, w + 2), np.uint8)
            cv2.floodFill(mask, mask_flood, (int(cX), int(cY)), new_val)
            new_val += 1
            x_blob.append([])
            y_blob.append([])

        for j in range(h):
            for k in range(w):
                if (mask[j][k] != 255) & (mask[j][k] != 0):
                    x_blob[mask[j][k] - 1].append(k)
                    y_blob[mask[j][k] - 1].append(j)

        characteristics = img_characteristics(x_blob, y_blob, h, w)
        file_path = os.path.join(dest, filename.split(file_extension)[0] + '.txt')
        file = open(file_path, "w")
        for cX, cY, width, height in characteristics:
            file.write("{} {} {} {} {}".format(object_class, cX, cY, width, height) + "\n")
        file.close()
        if i/files_number*toolbar_width > bar_progress:
            sys.stdout.write("-")
            sys.stdout.flush()
            bar_progress += 1

    sys.stdout.write("]\n")
    print("Found nothing on ", empty_files_count, " masks")


if __name__ == "__main__":
    args = parse_args()
    process(args.gt_path, args.object_class, args.dest)
