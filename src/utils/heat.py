# ------------------------------------------
#               Projet 2A
#
# Author : Antoine Prieur / Quentin Fleury
#
# Creation : 30/01/19
#
# File : heat.py
#
# Last modification : Antoine Prieur
#
# ------------------------------------------

from copy import deepcopy
import numpy as np

square_size = (86, 86)
padding = (40, 40)


def quadrilateral(img, x, y, s, color=0):

    """

    Creates a copy of the image with a quadrilateral to the given coordinates

    :param img: matrix corresponding to an image
    :param x: x-coordinate of the square (center of the square)
    :param y: y-coordinate of the square (center of the square)
    :param s: size of the square
    :param color: color of the square (default to 0 == black)
    :return: copy of the matrix with the quadrilateral

    """

    size = (img.shape[0], img.shape[1])
    ret = deepcopy(img)
    for i in range(x-s[0]//2, x+s[0]//2):
        for j in range(y-s[1]//2, y+s[1]//2):
            if ((i >= 0) & (i < size[0])) & ((j >= 0) & (j < size[1])):
                ret[i][j] = color
    return ret


def img_heat(img, model, s_size=square_size):

    """

    Creates the heat map of an image

    :param img: matrix corresponding to an image
    :param model: Keras model we want to use to make the predictions_3_classes
    :param s_size: size of the square we will use to sweep our image
    :return: matrix corresponding to the heat map of our image

    """
    size_x = img.shape[0]
    size_y = img.shape[1]
    test = []
    res = np.zeros((size_x//padding[0]+1, size_y//padding[1]+1))
    (i, j) = (0, 0)
    (counter_x, counter_y) = (0, 0)
    while i < size_x:
        while j < size_y:
            x_test = quadrilateral(img, i, j, s_size)
            prediction = model.predict(x_test[None, ...])
            test.append(prediction)
            res[counter_x][counter_y] = prediction[0][0]
            j += padding[0]
            counter_y += 1
        i += padding[1]
        counter_x += 1
        j = 0
        counter_y = 0
    return res
