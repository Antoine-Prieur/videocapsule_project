# ------------------------------------------
#               Projet 2A
#
# Author : Quentin Fleury
#
# Creation : 19/03/19
#
# File : mask.py
#
# Last modification : Quentin Fleury
#
# ------------------------------------------

import cv2
import numpy as np


def circle(im_size, radius):
    mask = np.zeros(im_size, np.float64)
    center = (im_size[0]//2, im_size[1]//2)
    cv2.circle(mask, center, )