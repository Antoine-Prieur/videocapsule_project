# --------------------------------------
#               Projet 2A
#
# Author : Antoine Prieur
#
# Creation : 05/07/19
#
# File : performance_evaluation.py
#
# Last modification : Antoine Prieur
#
# Evaluate the performances of a YoloV3 network
#
# ------------------------------------------


import argparse
import os
import sys
import numpy as np


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("results_path", help="path to the .txt file containing the prediction")
    parser.add_argument("names_path", help="path to the .names file")
    args = parser.parse_args()

    error_flag = 0
    if not os.path.exists(args.results_path):
        print("The path " + args.results_path + " does not exist")
        error_flag = 1
    if not os.path.exists(args.names_path):
        print("The path " + args.names_path + " does not exist")
        error_flag = 1
    if error_flag:
        sys.exit(1)
    return args


def process(results_path, names_path):
    f = open(results_path, "r")
    f2 = open(names_path, 'r')
    lines = f.read().split("\n")
    classes = f2.read().split("\n")
    if "" in classes:
        classes.remove("")

    i = 2

    object_count = 0
    empty_count = 0

    perfect = 0
    class_perfect = np.zeros(len(classes))
    class_detail = np.zeros(len(classes))
    detected_correctly = 0
    not_detected_correctly = 0

    while i < len(lines) - 1:

        # -- Ground truth --

        ground_truth = []
        label_file, _ = os.path.splitext(lines[i].split("Enter Image Path: ")[1].split(": Predicted")[0])
        label_file = label_file + ".txt"
        if os.path.exists(label_file):
            f_label = open(label_file, "r")
            lines_gt = f_label.read().split("\n")
            if "" in lines_gt:
                lines_gt.remove("")
            for gt in lines_gt:
                ground_truth.append(int(gt.split(" ")[0]))

        # ----

        # -- Prediction --

        j = 1
        detected_classes = []
        while not("Enter Image Path" in lines[i+j]):
            if not("BB:" in lines[i+j]):
                detected_classes.append(classes.index(lines[i+j].split(":")[0]))
            j += 1
        i += j

        # ----

        if len(ground_truth) != 0:
            object_count += 1
            class_detail[ground_truth[0]] += 1
            if len(detected_classes) != 0:
                detected_correctly += 1
                if detected_classes.count(ground_truth[0]) == ground_truth.count(ground_truth[0]):
                    perfect += 1
                    class_perfect[ground_truth[0]] += 1
        else:
            empty_count += 1
            if len(detected_classes) == 0:
                not_detected_correctly += 1
    print("\n------------ Results ------------\n")
    print("File used: " + results_path + "\n\n")
    print("Perfect detection (global): " + str(100*perfect/object_count) + "%\n")
    for idx, label in enumerate(classes):
        print("   " + classes[idx] + ": " + str(100*class_perfect[idx] / class_detail[idx]) + "%\n")
    print("Detected something (and had to detect something): " + str(100*detected_correctly / object_count) + "%\n")
    print("Detected nothing (and had to detect nothing): " + str(100*not_detected_correctly / empty_count) + "%\n")


if __name__ == "__main__":
    args = parse_args()
    process(args.results_path, args.names_path)

# process('../darknet/results/results.txt', '../darknet/pathologies.names')
