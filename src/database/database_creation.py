# ------------------------------------------
#               Projet 2A
#
# Author : Antoine Prieur / Quentin Fleury
#
# Creation : 20/11/18
#
# File : database_creation.py
#
# Last modification : Antoine Prieur
#
# ------------------------------------------

# Libraries

from src.utils.import_img import import_image

import numpy as np

# ----

# ---- Definition of the methods ----


def create_database(im_size, data_path, crop_size=(0, 0)):

    """

    Return in a list of array the images of the given path

    :param im_size: We define the size of the image we want to import
    :param data_path: The path of the images
    :param crop_size: If you want to crop the borders of the images
    :return: The list of all the images

    """

    x_database = import_image(im_size, data_path, crop_size=crop_size)
    x_database = np.array(x_database, dtype=np.float32)
    x_database /= 255
    return x_database

# ----
