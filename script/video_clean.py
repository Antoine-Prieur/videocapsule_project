# --------------------------------------
#               Projet 2A test
#
# Author : Antoine Prieur
#
# Creation : 24/04/19
#
# File : video_clean.py
#
# Last modification : Antoine Prieur
#
# Imports videos, convert them to image, make a prediction
# of all the images of the videos and save the results into
# a CSV file
#
# ------------------------------------------

from src.database.database_generator import DatabaseGenerator
from src.models import clean_unclean

from src.utils.video_conv import mpg_to_png
import os
import csv

videos_path = '../../../../../run/media/antoprie67/Passport Ext4/todo/'
done_path = '../../../../../run/media/antoprie67/Passport Ext4/done/'
save_path = '../data/tmp/'

list_dir = os.listdir(videos_path)

model = clean_unclean.architecture((224, 224))
model.load_weights('../backup/weights_clean_classification/weights-clean-improvement-16-0.95.hdf5')

tmp = os.listdir(save_path)
for j in tmp:
    file = os.path.join(save_path, j)
    os.remove(file)

with open('../backup/clean_results/results.csv', mode='a') as results:
    writer = csv.writer(results, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    for i in list_dir:
        video_path = os.path.join(videos_path, i)
        video_done_path = os.path.join(done_path, i)
        save_format = os.path.join(save_path, 'output_%04d.png')
        mpg_to_png(video_path, save_format)
        os.rename(video_path, video_done_path)
        generator = DatabaseGenerator((224, 224), [save_path], 2, batch_size=8,
                                      crop_size=(32, 32), randomize=False)
        prediction = model.predict_generator(generator=generator, workers=4, verbose=1)
        tmp = os.listdir(save_path)
        for j in tmp:
            file = os.path.join(save_path, j)
            os.remove(file)
        res = [i]
        for j in range(len(prediction)):
            res.append(prediction[j][0])
        writer.writerow(res)
