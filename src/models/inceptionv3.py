# ------------------------------------------
#               Projet 2A
#
# Author : Antoine Prieur
#
# Creation : 19/06/2019
#
# File : inceptionv3.py
#
# Last modification : Antoine Prieur
#
# ------------------------------------------

from keras.layers import GlobalAveragePooling2D
from keras.layers import Dense
from keras.models import Model

from keras.applications.inception_v3 import InceptionV3


def architecture(nb_classes):
    base_model = InceptionV3(weights='imagenet', include_top=False)

    x = base_model.output
    x = GlobalAveragePooling2D()(x)
    x = Dense(1024, activation='relu')(x)
    output = Dense(nb_classes, activation='softmax')(x)

    # this is the model we will train
    model = Model(inputs=[base_model.input], outputs=[output])

    for layer in base_model.layers:
        layer.trainable = False

    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    model.summary()

    return model

