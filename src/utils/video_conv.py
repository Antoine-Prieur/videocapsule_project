# ------------------------------------------
#               Projet 2A
#
# Author : Antoine Prieur / Quentin Fleury
#
# Creation : 27/11/18
#
# File : video_conv.py
#
# Last modification : Antoine Prieur
#
# ------------------------------------------

# Libraries

from ffmpy import FFmpeg

# ----


def mpg_to_png(video_path, save_path):

    """
    usage : mpg_to_png('../../data/video_1/video1.mpg', '../../data/video_1/output_%04d.png')
    :param save_path: The path where we save the images
    :param video_path: The path of the video
    :return: A list of .png
    """

    ff = FFmpeg(

        inputs={video_path: None},
        outputs={save_path: '-r 0.25'}
    )

    print(ff.cmd)
    ff.run()

