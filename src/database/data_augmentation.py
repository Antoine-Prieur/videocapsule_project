# ------------------------------------------
#               Projet 2A
#
# Author : Antoine Prieur
#
# Creation : 23/04/19
#
# File : database_augmentation.py
#
# Last modification : Antoine Prieur
#
# Regroup all the data augmentation class
#
# ------------------------------------------

import cv2
import random


def process(img, modifiers):

    """

    :param img: a matrix of an image
    :param modifiers: list of all the modifications we want to apply to the image
    :return: the modified image

    """

    for type in modifiers:
        img = type.compute(img)
    return img


class Rotate:

    """
    Rotate an image in a random direction
    """

    def __init__(self, prob=1, rotation=None):
        self.rotation = rotation
        self.prob = prob

    def compute(self, img):
        if random.random() < self.prob:
            rows, cols, channel = img.shape
            if self.rotation is None:
                M = cv2.getRotationMatrix2D((cols / 2, rows / 2), random.random() * 380, 1)
                return cv2.warpAffine(img, M, (cols, rows))
            else:
                M = cv2.getRotationMatrix2D((cols / 2, rows / 2), self.rotation * 380, 1)
                return cv2.warpAffine(img, M, (cols, rows))
        else:
            return img


class Flip:

    """
    Flip an image in a random direction
    """

    def __init__(self, prob=1/3):
        self.prob = prob

    def compute(self, img):
        if random.random() < self.prob:
            flip = random.choice([0, 1])
            return cv2.flip(img, flip)
        else:
            return img


class ColorJittering:

    """
    Invert the color channels
    """

    def __init__(self, prob=0.9):
        self.prob = prob

    def compute(self, img):
        if random.random() < self.prob:
            channels = [0, 1, 2]
            random.shuffle(channels)
            channel_r = img[:, :, channels[0]].copy()
            channel_g = img[:, :, channels[1]].copy()
            channel_b = img[:, :, channels[2]].copy()
            img[:, :, 0] = channel_r
            img[:, :, 1] = channel_g
            img[:, :, 2] = channel_b
            return img
        else:
            return img
