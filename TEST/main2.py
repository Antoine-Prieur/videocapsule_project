# --------------------------------------
#               Projet 2A
#
# Author : Antoine Prieur / Quentin Fleury
#
# Creation : 18/12/18
#
# File : main.py
#
# Last modification : Antoine Prieur
#
# ------------------------------------------

# -- Libraries --

from src.database.database_generator import DatabaseGenerator
from src.database.data_augmentation import Flip
from src.database.data_augmentation import Rotate
from src.models import binary_classification
from src.models import clean_unclean
from keras.callbacks import ModelCheckpoint
from src.utils.read_truth_csv import read_csv
from src.utils.roc import plot_roc
from src.utils.import_img import import_images
import numpy as np
from keras.utils.np_utils import to_categorical


import tensorflow as tf

# ----

batch_size = 8

y_true = read_csv('../data/GT_7.csv')

# Parameters
with tf.device('/gpu:0'):
    data_augmentation_1 = Flip(0.25)
    data_augmentation_2 = Rotate(0.25)
    training_generator = DatabaseGenerator((224, 224), ['../data/database_clean/training'], 2, y_true=y_true[0:500],
                                           batch_size=batch_size, crop_size=(32, 32), randomize=True, data_augmentation=
                                           [data_augmentation_1, data_augmentation_2])

    validation_generator = DatabaseGenerator((224, 224), ['../data/database_clean/testing'], 2, y_true=y_true[500:],
                                             batch_size=batch_size, crop_size=(32, 32), randomize=False)

    model = clean_unclean.architecture((224, 224))

    model.load_weights('../backup/weights_clean_classification/weights-clean-improvement-16-0.95.hdf5')

    prediction = model.predict_generator(generator=validation_generator, workers=4, verbose=1)

    plot_roc(to_categorical(y_true[500:], num_classes=2)[:, 1], prediction[:, 1])

    # file_path = "../backup/weights_binary_classification/weights-clean-improvment-{epoch:02d}-{val_acc:.2f}.hdf5"
    # checkpoint = ModelCheckpoint(file_path, monitor='val_acc', verbose=1, save_best_only=True, mode='max')
    #
    # model.fit_generator(generator=training_generator,
    #                     validation_data=validation_generator,
    #                     workers=4,
    #                     epochs=50,
    #                     verbose=1,
    #                     callbacks=[checkpoint])

#
# res = model.predict(x_database, batch_size=10)
#
# count = 0
# for i in range(0, len(res)):
#     if res[i][1] > 0.85:
#         count += 1
#
# print(count/len(res))
