import os
import sys
import random
import glob

args = sys.argv

data_path = args[1]
distribution = args[2]

list_dir = glob.glob(data_path+'*[0-9].jpg')
f1 = open("train.txt", "a")
f2 = open("test.txt", "a")

random.shuffle(list_dir)

for i in range(int(float(distribution)*len(list_dir))):
    path = os.path.join("data/", list_dir[i])
    f1.write(path+"\n")

for i in range(int(float(distribution)*len(list_dir)),len(list_dir)):
    path = os.path.join("data/", list_dir[i])
    f2.write(path+"\n")
