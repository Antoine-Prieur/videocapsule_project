# ------------------------------------------
#               Projet 2A
#
# Author : Antoine Prieur / Quentin Fleury
#
# Creation : 18/12/18
#
# File : pathologies_classification.py
#
# Last modification : Quentin Fleury
#
# ------------------------------------------

# -- Libraries --

from src.models.resnet18 import ResNet18
from keras.applications.inception_v3 import InceptionV3

# ----

#

# -- Architecture definition


def architecture(im_size, classes):

    """
    Return the current architecture of the clean/unclean classification

    :param im_size: the size of the image
    :param classes: the number of classes
    :return: the model of the architecture

    """
    model = ResNet18(include_top=True, input_shape=im_size + (3,), classes=classes, weights=None, pooling='avg')
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

    return model


# ----


