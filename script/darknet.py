# --------------------------------------
#               Projet 2A
#
# Author : Antoine Prieur
#
# Creation : 08/07/19
#
# File : darknet.py
#
# Last modification : Antoine Prieur
#
# ------------------------------------------

import argparse
import os
import sys
import cv2
from PIL import Image

sys.path.append('../')

from src.utils.bounding_box import draw_bb
from src.utils.import_img import import_image

MINIMUM_SIZE = (0.04, 0.04)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("data_path", help="path to the .data file")
    parser.add_argument("cfg_path", help="path to the .cfg file")
    parser.add_argument("weights_path", help="path to the .weights file")
    parser.add_argument("thresh", help="the threshold between 0 and 1", type=float)
    parser.add_argument("test_path", help="path to the .txt file we want to predict")
    parser.add_argument("result_path", help="path to the directory where we want to save the results")
    parser.add_argument("--dont_predict", help="if the prediction is already done in the results.txt file, "
                                               "use this arg", action="store_true")
    parser.add_argument("--dont_save", help="use this to not save the images with the BB", action="store_true")

    args = parser.parse_args()

    error_flag = 0
    if not os.path.exists(args.data_path):
        print("The path " + args.data_path + " does not exist")
        error_flag = 1
    if not os.path.exists(args.cfg_path):
        print("The path " + args.cfg_path + " does not exist")
        error_flag = 1
    if not os.path.exists(args.weights_path):
        print("The path " + args.weights_path + " does not exist")
        error_flag = 1
    if not os.path.exists(args.test_path):
        print("The path " + args.test_path + " does not exist")
        error_flag = 1
    if not os.path.exists(args.result_path):
        print("The path " + args.result_path + " does not exist")
        error_flag = 1
    if not ((args.thresh >= 0) and (args.thresh <= 1)):
        print("The threshold must be a float between 0 and 1")
        error_flag = 1
    if error_flag:
        sys.exit(1)
    return args


def process(data_path, cfg_path, weights_path, thresh, test_path, result_path, predict=True, save=True):

    """

    Make the prediction of several files (optional with the argument predict), create a file results.txt in result_path
    Then, create a directory "predictions_3_classes" in result_path where it saves the images with there bounding box based on the
    file results.txt

    """

    os.chdir("../darknet/")

    result_file = os.path.join(result_path, "results.txt")

    if predict:
        command = "./darknet detector test " + data_path + " " + cfg_path + " " + weights_path + " -thresh " \
                  + str(thresh) + " -dont_show < " + test_path + " > " + result_file
        os.system(command)

    if save:
        predictions_path = os.path.join(result_path, "predictions/")
        if not os.path.exists(predictions_path):
            os.mkdir(predictions_path)

        f = open(result_file, "r")
        lines = f.read().split("\n")

        classes = []
        i = 2

        while i < len(lines) - 1:

            probability = []
            label = []
            bb = []

            label_file, extension_file = os.path.splitext(lines[i].split("Enter Image Path: ")[1].split(": Predicted")[0])
            img = import_image((512, 512), label_file + extension_file)

            j = 1

            while not("Enter Image Path" in lines[i+j]):

                if not ("BB:" in lines[i + j]):
                    label.append(lines[i+j].split(":")[0])
                    p = int(lines[i+j].split(": ")[1].split("%")[0])/100
                    probability.append(p)
                else:
                    bb.append(lines[i+j].split("BB: ")[1])

                j += 1

            i += j

            for k in range(len(bb)):

                bb_coord = bb[k].split(" ")
                if not label[k] in classes:
                    classes.append(label[k])

                if (float(bb_coord[2]) > MINIMUM_SIZE[0]) & (float(bb_coord[2]) > MINIMUM_SIZE[1]):
                    res = draw_bb(img, float(bb_coord[0]), float(bb_coord[1]), float(bb_coord[2]), float(bb_coord[3]),
                                  label[k], probability[k], classes.index(label[k]))
                    res = Image.fromarray(cv2.cvtColor(res, cv2.COLOR_BGR2RGB))
                    res.save(os.path.join(predictions_path, label_file.split("/")[-1]) + ".jpeg")


if __name__ == "__main__":
    args = parse_args()
    process(args.data_path, args.cfg_path, args.weights_path, args.thresh, args.test_path, args.result_path,
            not args.dont_predict, not args.dont_save)
