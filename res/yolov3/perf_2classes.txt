
------------ Results ------------

File used: ../darknet/results/results.txt


Perfect detection (global): 57.5875486381323%

   inflammatory: 35.43307086614173%

   vascular: 79.23076923076923%

Detected something (and had to detect something): 71.98443579766537%

Detected nothing (and had to detect nothing): 95.58823529411765%

