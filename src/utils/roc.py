# ------------------------------------------
#               Projet 2A
#
# Author : Antoine Prieur / Quentin Fleury
#
# Creation : 20/11/18
#
# File : roc.py
#
# Last modification : Antoine Prieur
#
# ------------------------------------------

# Libraries

from matplotlib import pyplot as plt
from sklearn.metrics import roc_curve

# ----


def plot_roc(y_true, y_pred):
    fpr_keras, tpr_keras, threeholds_keras = roc_curve(y_true, y_pred)

    plt.figure(1)
    plt.plot([0, 1], [0, 1], 'k--')
    plt.plot(fpr_keras, tpr_keras)
    plt.xlabel('True positive')
    plt.ylabel('False positive')
    plt.title('ROC Curve')
    plt.legend(loc='best')
    plt.show()