# ------------------------------------------
#               Projet 2A
#
# Author : Antoine Prieur / Quentin Fleury
#
# Creation : 20/12/18
#
# File : clean_unclean.py
#
# Last modification : Antoine Prieur
#
# ------------------------------------------

# -- Libraries --

from keras.layers import Dense, Dropout, Flatten
from keras.layers import Convolution2D, MaxPooling2D, BatchNormalization

from keras.models import Sequential

# ----

#

# -- Architecture definition


def architecture(im_size):

    """
    Return the current architecture of the clean/unclean classification

    :param im_size: the size of the image
    :return: the model of the architecture

    """
    model = Sequential()

    model.add(Convolution2D(32, (3, 3), activation='relu', input_shape=im_size+(3,)))
    model.add(Convolution2D(64, (3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Convolution2D(64, (3, 3), activation='relu'))
    model.add(Convolution2D(128, (3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Flatten())
    model.add(Dense(64, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(2, activation= 'softmax'))

    model.compile(loss='categorical_crossentropy',
                  optimizer='adam',
                  metrics=['accuracy'])

    model.summary()
    return model


# ----
