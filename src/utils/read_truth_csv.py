# ------------------------------------------
#               Projet 2A
#
# Author : Antoine Prieur / Quentin Fleury
#
# Creation : 20/11/18
#
# File : read_truth_csv.py
#
# Last modification : Antoine Prieur
#
# ------------------------------------------

# -- Libraries --

import csv
import numpy as np

# ----


def read_csv(csv_name):
    with open(csv_name, 'r') as f:
        reader = csv.reader(f)
        my_list = list(reader)
        final_list = []
        for i in range(0, len(my_list) - 1):
            final_list.append(my_list[i][0].split(';')[1])
        final_list = np.array(final_list, dtype=int)
        return np.array(final_list)
