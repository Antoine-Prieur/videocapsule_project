# ------------------------------------------
#               Projet 2A
#
# Author : Antoine Prieur / Quentin Fleury
#
# Creation : 21/12/18
#
# File : randomize.py
#
# Last modification : Antoine Prieur
#
# ------------------------------------------

# -- Libraries --

import numpy as np

# ----


def shuffle(database, truth):
    if len(database) == len(truth):
        new_database = []
        new_truth = []
        index = np.arange(len(database))
        np.random.shuffle(index)
        for i in range(len(database)):
            new_database.append(database[index[i]])
            new_truth.append(truth[index[i]])
        return new_database, new_truth
    else:
        print("Database and truth must be the same length")
        return database, truth
