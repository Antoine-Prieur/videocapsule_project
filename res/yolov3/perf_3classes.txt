
------------ Results ------------

File used: ../darknet/results/results.txt


Perfect detection (global): 32.72251308900523%

   inflammatory: 17.322834645669293%

   vascular: 71.53846153846153%

   blood: 8.0%

Detected something (and had to detect something): 42.40837696335078%

Detected nothing (and had to detect nothing): 100.0%

