# --------------------------------------
#               Projet 2A
#
# Author : Antoine Prieur
#
# Creation : 14/06/19
#
# File : bounding_box.py
#
# Last modification : Antoine Prieur
#
# Utils to evaluate the efficiency of localization networks
#
# ------------------------------------------

import numpy as np
import cv2

COLORS = ((0, 255, 0), (0, 255, 255), (255, 0, 255), (204, 255, 0))


def iou(bb_origin, bb_size, p_origin, p_size):

    """
    Returns the Intersection Over Union (IoU)
    :param bb_origin: Coordinates of the origin (the top left corner) of the true Bounding Box (x, y)
    :param bb_size: Size of the true Bounding Box (x, y)
    :param p_origin: Coordinates of the origin (the top left corner) of the Predicted Bounding Box (x, y)
    :param p_size: Size of the Predicted Bounding Box (x, y)
    :return:
    """

    x_points = np.array([bb_origin[0], bb_origin[0]+bb_size[0], p_origin[0], p_origin[0]+p_size[0]])
    y_points = np.array([bb_origin[1], bb_origin[1]+bb_size[1], p_origin[1], p_origin[1]+p_size[1]])

    x_points.sort()
    y_points.sort()

    if (x_points[1] == bb_origin[0] + bb_size[0]) | (x_points[1] == p_origin[0] + p_size[0]):
        return 0
    if (y_points[1] == bb_origin[1] + bb_size[1]) | (y_points[1] == p_origin[1] + p_size[1]):
        return 0
    return (x_points[2] - x_points[1])*(y_points[2] - y_points[1]) / \
           (bb_size[0]*bb_size[1] + p_size[0]*p_size[1] - (x_points[2] - x_points[1])*(y_points[2] - y_points[1]))


def draw_bb(img, x, y, w, h, label, probability, class_number):

    """
    Returns an image with a bounding box
    :param img: The image
    :param x: Center x of the BB
    :param y: Center y of the BB
    :param w: Width of the BB
    :param h: Heigth of the BB
    :param label: The label of the BB
    :param probability: The probability of the prediction for this BB
    :param class_number: The number of the class (from 0 to ...) : changes the color
    :return:
    """
    (im_x, im_y, _) = img.shape

    res = cv2.rectangle(img, (int(im_x*(x-w/2)), int(im_y*(y-h/2))), (int(im_x*(x+w/2)), int(im_y*(y+h/2))),
                        COLORS[class_number % len(COLORS)], 2)
    font = cv2.FONT_HERSHEY_SIMPLEX
    res = cv2.putText(res, label+" "+str(probability*100)+" %", (int(im_x*(x-w/2)), int(im_y*(y-h/2))-10), font, 0.5,
                      COLORS[class_number % len(COLORS)], lineType=cv2.LINE_AA)
    return res


# img = import_image((512, 512), '../../data/wetransfer-32280a/bekhoukh 2013_05_28 ( 02.46.07 ).jpg')
# draw_bb(img, 0.5, 0.5, 0.04, 0.04, "vascular", 0.5)

