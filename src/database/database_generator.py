# ------------------------------------------
#               Projet 2A
#
# Author : Antoine Prieur
#
# Creation : 24/12/18
#
# File : database_generator.py
#
# Last modification : Antoine Prieur
#
# ------------------------------------------

import os
import numpy as np
import sys

from keras.utils.np_utils import to_categorical
from keras.utils import Sequence

sys.path.append('../../')

from src.utils.randomize import shuffle
from src.utils.import_img import import_images


class DatabaseGenerator(Sequence):

    """
    Generates the data for the training.

    We use this generator if we have a big dataset, it permits to avoid the save of all the images directly in the
    RAM of the GC, by generating the images batch per batch.

    :param im_size: a tuple representing the size of our image
    :param data_path: a list of all the path to the images we want to use. If we don't enter a ground_truth,
    the function will define a different class object for each path.
    :param n_classes: the number of class we are using
    :param crop_size: a tuple of int, has to be define if we want to crop the borders of the image
    :param randomize: a boolean, put it to True if you want to randomize the data at the beginning of every epoch
    :param batch_size: a int representing the size of our batch
    :param y_true: a list representing the ground truth : must be same length than the number of images, you must
    name correctly your files so that the ground truth corresponds to the right image (instead of 1.jpg, 2.jpg, ...,
    10.jpg, you should use 01.jpg, 02.jpg, ..., 10.jpg). If you use different paths, just enter the ground_truth in the
    same list : the images of the 1st path will be imported first (the 1st path in the list data_path), then the 2nd ...
    So make sure to name the files correctly and to enter the paths in data_path in the right order.
    :param data_augmentation: a list of the objects for the data_augmentation. To see all the available objects, check
    in src/database/data_augmentation.py. You must first initialize your object, and then add it to the parameter
    data_augmentation.
    """
    def __init__(self, im_size, data_path, n_classes, crop_size=(0, 0), randomize=True, batch_size=16, y_true=None,
                 data_augmentation=None):
        self.im_size = im_size
        self.data_path = data_path  # a tab of all the different paths
        self.crop_size = crop_size
        self.batch_size = batch_size
        self.randomize = randomize  # boolean : do we want to randomize the datas or not

        self.n_classes = n_classes  # the number of different classes
        self.n_index = []
        for i in range(len(data_path)):  # we append to a tab the number of images of each class
            self.n_index.append(len(os.listdir(data_path[i])))
        self.samples = sum(self.n_index)  # the full number of samples
        self.index = np.arange(self.samples)  # an array containing 0 to samples
        self.data_augmentation = data_augmentation

        if y_true is None:  # if we entered the truth, we take it, else we define the truth thanks to the
            # structure
            self.y_true = np.zeros(self.n_index[0])
            if len(self.n_index) > 1:
                count = 1
                for i in range(1, len(self.n_index)):
                    x = np.zeros(self.n_index[i])
                    self.y_true = np.concatenate((self.y_true, x+count))
                    count += 1
        else:
            self.y_true = y_true
        if self.randomize:
            [self.index, self.y_true] = shuffle(self.index, self.y_true)

    def __len__(self):
        if np.floor(float(self.samples)/float(self.batch_size)) == float(self.samples)/float(self.batch_size):
            return int(np.floor(float(self.samples) / float(self.batch_size)))
        else:
            return int(np.floor(float(self.samples)/float(self.batch_size)))+1

    def __getitem__(self, idx):
        if self.batch_size*(idx+1) > self.samples:
            idx_final = self.samples
        else:
            idx_final = self.batch_size*(idx+1)
        x_database = import_images(self.im_size, self.data_path, self.index, [idx*self.batch_size, idx_final],
                                   self.n_index, crop_size=self.crop_size, modifiers=self.data_augmentation)
        x_database = np.array(x_database, dtype=np.float32)
        x_database /= 255

        return x_database, to_categorical(self.y_true[idx*self.batch_size: idx_final],
                                          num_classes=self.n_classes)

    def on_epoch_end(self):
        if self.randomize:
            [self.index, self.y_true] = shuffle(self.index, self.y_true)  # method which shuffles the database and the
            # truth