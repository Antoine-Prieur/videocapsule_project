# ------------------------------------------
#               Projet 2A
#
# Author : Antoine Prieur / Quentin Fleury
#
# Creation : 30/01/19
#
# File : binary_classification.py
#
# Last modification : Antoine Prieur
#
# ------------------------------------------

# -- Libraries --

from keras.layers import Dense
from keras.layers import MaxPooling2D, GlobalMaxPooling2D, Flatten, Dropout, GlobalAveragePooling2D, BatchNormalization, Conv2D, Activation, AveragePooling2D

from keras.models import Sequential

import tensorflow as tf

# ----

#

# -- Architecture definition


def architecture(im_size):

    """
    Return the current architecture of the clean/unclean classification

    :param im_size: the size of the image
    :return: the model of the architecture

    """

    model = Sequential()

    # model.add(Conv2D(32, (10, 10), padding='same', input_shape=im_size + (3,)))
    # model.add(BatchNormalization())
    # model.add(Activation('relu'))
    # model.add(Conv2D(64, (5, 5), padding='same'))
    # model.add(BatchNormalization())
    # model.add(Activation('relu'))
    # model.add(MaxPooling2D(pool_size=(2, 2)))
    # model.add(Conv2D(32, (10, 10), padding='same', input_shape=im_size + (3,)))
    # model.add(BatchNormalization())
    # model.add(Activation('relu'))
    # model.add(Conv2D(64, (5, 5), padding='same'))
    # model.add(BatchNormalization())
    # model.add(Activation('relu'))
    # model.add(GlobalAveragePooling2D())
    # model.add(Dense(2, activation= 'softmax'))

    model.add(Conv2D(32, (10, 10), padding='same', activation='relu', input_shape=im_size + (3,)))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(4, 4)))
    model.add(Conv2D(64, (3, 3), activation='relu', padding='same'))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Conv2D(128, (3, 3), activation='relu', padding='same'))
    model.add(BatchNormalization())
    model.add(GlobalAveragePooling2D())
    model.add(Dense(32, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(2, activation= 'softmax'))

    model.compile(loss='categorical_crossentropy',
                  optimizer='adam',
                  metrics=['accuracy'])

    model.summary()
    return model


# ----


