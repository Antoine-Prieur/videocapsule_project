# Introduction to the project

Analysis of images taken by videocapsule

The goal of the project is to find a way to analyse images of the small intestin using CNN.

You can find in the sources some python files to implements CNN architectures using Keras and TensorFlow. 

You can also find a clone of the YoloV3 git in the _darknet_ directory. In this, you can find pathologies_x.cfg, .names and .data files, used to make a training using yolov3_tiny_weights for the small intestin classes.
There are also some custom scripts for YoloV3 detailled below in _/videocapsule_projects/scripts_. 

Antoine Prieur
antoine.prieur45@gmail.com

# Scripts tutorial

We will detail the different scripts in the directory _scripts_.

-------------------------

The script ```darknet.py``` is used to automatically start a YoloV3 prediction on several images. It will save all the predicted images with their bounding box. This script will create in the destination directory a directory called _predictions_ where it will list the predicted images with their bounding box, and it will create a file called _results.txt_ where you can see the text version of the results.

Examples of usage:

```
python darknet.py /path/to/.data/file /path/to/.cfg/file path/to/.weights/file 0.5 /path/to/your/images/in/.txt/file /path/to/destination/directory/
python darknet.py /path/to/.data/file /path/to/.cfg/file path/to/.weights/file 0.7 /path/to/your/images/in/.txt/file /path/to/destination/directory/ --dont_predict
python darknet.py /path/to/.data/file /path/to/.cfg/file path/to/.weights/file 0.9 /path/to/your/images/in/.txt/file /path/to/destination/directory/ --dont_save
```

In the first example, we will make the predictions and save the images with there bounding box with a threshold of 0.5.
In the second one, the threshold is at 0.7, and we have the option --dont_predict. It permits to directly use the file _results.txt_ in the destination directory to create the directory with the predicted images with their bounding box.
In the last example, the threshold is at 0.9 and we have the option --dont_save. It permits to only generate the file _results.txt_ without creating the directory **predictions** with the predicted images and their bounding box.

The .txt file which contains the paths of your images must have this form :

```
/path/to/image/1
/path/to/image/2
/path/to/image/3
...
```

The .data and .cfg files are the one used by YoloV3.
This script also remove all the bounding box smaller than 4% of the size of the image, to modify this value, change the tuple **MINIMUM_SIZE**.

-----------------------

The script **database_preparation.py** is used to prepare a dataset for YoloV3. It permits to create the .txt files with the bounding box of a list of masks saved in a directory. It detects the blobs in the mask and create the corresponding bounding box, and save them to the YoloV3 format.
Examples of usage:

```
python database_prepation.py /path/to/masks/directory/ 0
python database_prepation.py /path/to/masks/directory/ 1 --dest /path/to/destination/directory/
```

In the first example, we will take all the masks in _path/to/masks/directory/_, and will create a .txt file in _/res/yolov3/preparation/_ with the class label 0.
In the second example, we take the same masks but we save them in _/path/to/save/directory/_ with the class label 1.

-----------------------

The script **performance_evaluation.py** is used to calculate the performances of a .weights file from YoloV3. It computes the performances on a .txt file where the results are stored. This .txt file can be generated with the script **darknet.py** (the _results.txt_ file in the destination directory). It doesn't generate any file, it only prints in the shell the results, check the example below to save them in a file.
Example of usage:
```
python performance_evaluation.py /path/to/results.txt/file /path/to/.names/files > /path/to/destination/file
```
In this example, we create the file _/path/to/save/file_ to save the predictions. The .names is the YoloV3 file where the names of the classes are stored.

------------------------

The script **script_video_extraction.py** is used to extract all the frames of a video, and it will also create a _.txt_ file which will store the path of all the frames in the format used in YoloV3. The _.txt_ will be stored in the destination directory, and the frames will be saved in a directory called _frames_ in the destination directory (the script will create it if it doesn't exists).
Example of usage:
```
python script_video_extraction.py /path/to/video5.mpg /path/to/destination/directory/
```
This example takes the video _/path/to/video5.mpg_, store the frames in _/path/to/destination/directory/frames/_ and creates the text file _/path/to/destination/directory/video5.txt_.
