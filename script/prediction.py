# --------------------------------------
#               Projet 2A
#
# Author : Antoine Prieur / Quentin Fleury
#
# Creation : 18/12/18
#
# File : main.py
#
# Last modification : Antoine Prieur
#
# ------------------------------------------

# -- Libraries --

import matplotlib.pyplot as plt
from src.database.database_generator import DatabaseGenerator
from src.models import binary_classification
from src.models import pathologies_classification
from src.utils.heat import img_heat
from src.utils.roc import plot_roc
from src.utils.import_img import import_image
import numpy as np
from PIL import Image
from keras.utils import to_categorical
from sklearn.metrics import confusion_matrix

from src.models import clean_unclean
from keras.callbacks import ModelCheckpoint

import tensorflow as tf

import cv2

# ----

batch_size = 6

# Parameters

config = tf.ConfigProto()
config.gpu_options.allow_growth = True
session = tf.InteractiveSession(config=config)

with tf.device('/gpu:0'):

    # validation_generator = DatabaseGenerator((512, 512), ['../data/Base_WCE/Testing/Normal_old',
    #                                                       '../data/Base_WCE/Testing/Vascular_Lesion'], 2,
    #                                          batch_size=batch_size, crop_size=(32, 32), randomize=False)

    # y_true = []
    # for i in range(1314):
    #     y_true.append(0)
    # for i in range(605):
    #     y_true.append(1)
    # for i in range(147):
    #     y_true.append(2)

    validation_generator = DatabaseGenerator((512, 512), ['../data/videos/El5aIw3300_P1/frames/'], 3,
                                             batch_size=batch_size, crop_size=(32, 32), randomize=False)

    model = pathologies_classification.architecture((512, 512), 3)
    #
    model.load_weights('../backup/weights_pathologies_classification/weights-normal-inflammatory-vascular-v2.hdf5')

    res = model.predict_generator(generator=validation_generator, workers=4, verbose=1)

    # y_true = to_categorical(y_true, num_classes=3)
    # confusion = confusion_matrix(y_true.argmax(axis=1), res.argmax(axis=1))
    # plot_roc(to_categorical(y_true, num_classes=2)[:, 0], res[:, 0])

    img = import_image((512, 512), '../data/wetransfer-32280a/bekhoukh 2013_05_28 ( 02.46.07 ).jpg', crop_size=(32, 32))
    #
    # img = np.array(img, np.float32)/255
    #
    # heat = img_heat(img, model)
    #
    # plt.imshow(heat)
    # plt.show()
    # plt.savefig("heat.jpg")

    # f, axarr = plt.subplots(2, 3)
    # axarr[0, 0].imshow(img)
    # axarr[0, 0].set_title("Inflammatory_Lesion")
    # axarr[0, 1].imshow(img2)
    # axarr[0, 1].set_title("Vascular_Lesion")
    # axarr[1, 0].imshow(img3)
    # axarr[1, 0].set_title("Normal")
    # plt.show()

    # from PIL import Image
    #
    # im = Image.fromarray((heat*255).astype(np.uint8))
    # im.show()
