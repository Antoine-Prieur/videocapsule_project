# --------------------------------------
#               Projet 2A
#
# Author : Antoine Prieur / Quentin Fleury
#
# Creation : 26/03/19
#
# File : encoding.py
#
# Last modification : Antoine Prieur
#
# ------------------------------------------

from scipy import ndimage
import numpy as np
from PIL import Image
import os


def decoding_rotation(image):
    (x, y) = tuple(np.array(image.shape[:2])//2)
    rot1 = ndimage.rotate(image, 45)
    rot2 = ndimage.rotate(rot1, -45)
    center = tuple(np.array(rot2.shape[:2]) // 2)
    res = rot2[center[0]-x: center[0]+x, center[1]-y:center[1]+y]
    return res


def decode_images(path, path_to, quality):
    list_dir = os.listdir(path)
    for i in range(len(list_dir)):
        img_path = os.path.join(path, list_dir[i])
        img = Image.open(img_path)
        img = np.array(img)
        res = decoding_rotation(img)
        res = Image.fromarray(res)
        res.save(path_to+"{0:003}".format(i)+".jpg", 'JPEG', quality=quality)


decode_images('../../data/Base_WCE/Training/Inflammatory_Lesion/', '../../data/Base_WCE_2/Training/Inflammatory_Lesion/', 90)
