# ------------------------------------------
#               Projet 2A
#
# Author : Antoine Prieur / Quentin Fleury
#
# Creation : 20/11/18
#
# File : import_img.py
#
# Last modification : Antoine Prieur
#
# ------------------------------------------

# Libraries

import os
import cv2

from src.database.data_augmentation import process
# ----


def import_images(im_size, data_path, database, bounds, n_index, crop_size=(0, 0), modifiers=None):

    """

    We use this function to import all the images of a path

    :param im_size: We define the size of the image we want to import
    :param data_path: The path of the images
    :param index:
    :param crop_size: Use it if you want to crop the borders of the images
    :param modifiers: tab of tuple containing the type and the probability of each type if data augmentation
    :return: The list of all the images between the two indexes

    """

    train_list = []
    for k in range(bounds[0], bounds[1]):
        j = database[k]
        database_index = 0
        for i in n_index:
            if j >= i:
                j = j - i
                database_index += 1
            else:
                break
        list_dir = os.listdir(data_path[database_index])
        img_path = os.path.join(data_path[database_index], list_dir[j])
        img = cv2.imread(img_path)
        x, y, channel = img.shape
        if im_size[0] > x | im_size[1] > y:
            im_size = (x, y)
        img = img[crop_size[0]:x-1-crop_size[0], crop_size[1]:y-1-crop_size[1]]
        img = cv2.resize(img, im_size)
        if modifiers is not None:
            img = process(img, modifiers)
        train_list.append(img)
    return train_list


def import_image(im_size, img_path, crop_size=(0, 0), modifiers=None):

    """

    We use this function to import all the images of a path

    :param im_size: We define the size of the image we want to import
    :param crop_size: Use it if you want to crop the borders of the images
    :param modifiers: Use it to modify the image (for data augmentation)
    :return: The list of all the images between the two indexes

    """

    img = cv2.imread(img_path)
    x, y, channel = img.shape
    if im_size[0] > x | im_size[1] > y:
        im_size = (x, y)
    img = img[crop_size[0]:x-1-crop_size[0], crop_size[1]:y-1-crop_size[1]]
    img = cv2.resize(img, im_size)
    if modifiers is not None:
        img = process(img, modifiers)
    return img
