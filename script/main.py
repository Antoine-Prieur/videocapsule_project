# --------------------------------------
#               Projet 2A
#
# Author : Antoine Prieur / Quentin Fleury
#
# Creation : 18/12/18
#
# File : main.py
#
# Last modification : Antoine Prieur
#
# Example on how to start a training using the generator and
# the data augmentation.
#
# ------------------------------------------

# -- Libraries --

from keras.callbacks import ModelCheckpoint
import sys

sys.path.append('../')

from src.database.database_generator import DatabaseGenerator
from src.models import pathologies_classification
from src.models import inceptionv3
from src.database.data_augmentation import Rotate
from src.database.data_augmentation import Flip
from src.database.data_augmentation import ColorJittering
from src.models import clean_unclean
from src.models import binary_classification

import tensorflow as tf

# ----

config = tf.ConfigProto()
config.gpu_options.allow_growth = True
session = tf.InteractiveSession(config=config)

# that's a test
batch_size = 6

# Parameters

# x = Rotate(1)
# y = Flip(0.75)
# z = ColorJittering(0.9)

training_generator = DatabaseGenerator((512, 512), ['../data/CADCAP/Training/Normal',
                                                    '../data/CADCAP/Training/Inflammatory_Lesion',
                                                    '../data/CADCAP/Training/Vascular_Lesion'], 3,
                                       batch_size=batch_size, crop_size=(32, 32))

validation_generator = DatabaseGenerator((512, 512), ['../data/CADCAP/Testing/Normal',
                                                      '../data/CADCAP/Testing/Inflammatory_Lesion',
                                                      '../data/CADCAP/Testing/Vascular_Lesion'], 3,
                                         batch_size=batch_size, crop_size=(32, 32), randomize=False)

file_path = "../backup/weights_pathologies_classification/weights-improvement-inception-{epoch:02d}-{val_acc:.2f}.hdf5"
checkpoint = ModelCheckpoint(file_path, monitor='val_acc', verbose=1, save_best_only=True, mode='max')

model = inceptionv3.architecture(3)
# model.load_weights('../backup/weights_pathologies_classification/weights-improvement-27-0.57.hdf5')

model.fit_generator(generator=training_generator, validation_data=validation_generator, workers=4, epochs=100,
                    verbose=1, callbacks=[checkpoint])
