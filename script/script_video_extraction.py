# --------------------------------------
#               Projet 2A
#
# Author : Antoine Prieur
#
# Creation : 08/07/19
#
# File : script_video_extraction.py
#
# Last modification : Antoine Prieur
#
# ------------------------------------------

import os
import sys
import argparse


def parse_args():
	parser = argparse.ArgumentParser()
	parser.add_argument("video_path", help="path to the video file")
	parser.add_argument("res_path", help="path to the directory we want to save the frames")
	args = parser.parse_args()

	error_flag = 0
	if not os.path.exists(args.video_path):
		print("The path " + args.video_path + " does not exist")
		error_flag = 1
	if not os.path.exists(args.res_path):
		print("The path " + args.res_path + " does not exist")
		error_flag = 1
	if error_flag:
		sys.exit(1)
	if not os.path.exists(os.path.join(args.res_path, "frames")):
		os.mkdir(os.path.join(args.res_path, "frames"))
	return args


def process(video_path, res_path):

	video_name, video_extension = os.path.splitext(video_path)

	frames_path = os.path.join(res_path, "frames/")

	command = "ffmpeg -i " + video_path + " -vf fps=5 " + frames_path +"thumb%05d.jpg -hide_banner"
	os.system(command)

	list_dir = os.listdir(frames_path)
	f = open(os.path.join(res_path, video_name.split("/")[-1]) + ".txt", "w")

	for i in list_dir:
		data_path = os.path.join(frames_path, i)
		f.write(data_path+"\n")


if __name__ == "__main__":
	args = parse_args()
	process(args.video_path, args.res_path)
